package com.example.springsocial.mapper.editor;

public interface Editor<T> {

	void edit(T toEdit, T editBy);

}
