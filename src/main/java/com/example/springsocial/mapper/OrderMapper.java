package com.example.springsocial.mapper;


import com.example.springsocial.mapper.decorator.OrderMapperDecorator;
import com.example.springsocial.model.Order;
import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.request.NewOrderDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
@DecoratedWith(OrderMapperDecorator.class)
public interface OrderMapper extends EditingMapper<Order> {

	@Mapping(target = "orderItems.order", ignore = true)
	OrderDto entityToDto(Order order);

	@Mapping(target = "orderItems.order", ignore = true)
	List<OrderDto> entitiesToDtos(List<Order> orderList);

	@Mapping(target = "id", ignore = true)
	Order createNewOrder (NewOrderDto orderDto);
}
