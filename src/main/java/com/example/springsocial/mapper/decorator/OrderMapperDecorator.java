package com.example.springsocial.mapper.decorator;

import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.mapper.OrderMapper;
import com.example.springsocial.model.Order;
import com.example.springsocial.model.OrderCustomer;
import com.example.springsocial.model.OrderItem;
import com.example.springsocial.model.dto.UserPrincipal;
import com.example.springsocial.model.dto.productMenuApi.ProductMenuPizzaDto;
import com.example.springsocial.model.dto.request.NewOrderDto;
import com.example.springsocial.model.enumerated.OrderState;
import com.example.springsocial.service.product.PizzaProductMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class OrderMapperDecorator extends EditingMapperImpl<Order> implements OrderMapper {

	@Autowired
	private OrderMapper delegate;

	@Autowired
	private PizzaProductMenuService productMenuService;

	@Override
	public Order createNewOrder(NewOrderDto orderDto) {
		List<UUID> pizzaIds = orderDto.getItems().stream().map(NewOrderDto.OrderItemDto::getPizzaId).collect(Collectors.toList());
		List<ProductMenuPizzaDto> pizzasByListOfIds = productMenuService.findPizzasByListOfIds(pizzaIds);
		Map<UUID, ProductMenuPizzaDto> pizzaDtoMap = pizzasByListOfIds.stream()
			.collect(Collectors.toMap(ProductMenuPizzaDto::getId, Function.identity()));
		AtomicReference<Double> totalSum = new AtomicReference<Double>(0d);
		List<OrderItem> orderItems = orderDto.getItems()
			.stream()
			.map(i -> {
				ProductMenuPizzaDto productMenuPizzaDto = pizzaDtoMap.get(i.getPizzaId());
				if (Objects.nonNull(productMenuPizzaDto)) {
					OrderItem orderItem = new OrderItem();
					orderItem.setAmount(i.getAmount());
					orderItem.setDescription(productMenuPizzaDto.getDescription());
					orderItem.setName(productMenuPizzaDto.getName());
					orderItem.setPrice(productMenuPizzaDto.getPrice());
					orderItem.setPizzaId(productMenuPizzaDto.getId());
					totalSum.updateAndGet(v -> v + i.getAmount() * productMenuPizzaDto.getPrice());
					return orderItem;
				}
				throw new BadRequestException("Something went wrong");
			})
			.collect(Collectors.toList());

		Order order = new Order();
		OrderCustomer orderCustomer = getOrderCustomer(orderDto.getCustomer());
		order.setCustomer(orderCustomer);
		order.setOrderItems(orderItems);
		order.setTotalPrice(totalSum.get());
		order.setCreatedAt(LocalDateTime.now());
		order.setState(OrderState.CREATED);
		order.prepareSave();
		return order;
	}

	public OrderCustomer getOrderCustomer(NewOrderDto.OrderCustomerDto orderCustomerDto) {
		UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext()
			.getAuthentication()
			.getPrincipal();
		return new OrderCustomer(
			userPrincipal.getAccount().getId(),
			orderCustomerDto.getFullname(),
			orderCustomerDto.getZip(),
			orderCustomerDto.getAddress(),
			orderCustomerDto.getCity(),
			orderCustomerDto.getEmail(),
			orderCustomerDto.getPhone()
		);
	}

}
