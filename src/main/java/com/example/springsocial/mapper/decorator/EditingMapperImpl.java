package com.example.springsocial.mapper.decorator;

import com.example.springsocial.mapper.EditingMapper;
import com.example.springsocial.mapper.editor.Editor;
import org.springframework.beans.factory.annotation.Autowired;

public class EditingMapperImpl<T> implements EditingMapper<T> {

	private Editor<T> editor;

	public void edit(T toEdit, T editBy) {
		editor.edit(toEdit, editBy);
	}

	@Autowired
	public void setOwnerEditor(Editor<T> editor) {
		this.editor = editor;
	}

}
