package com.example.springsocial.controller.impl;

import com.example.springsocial.controller.OrderController;
import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PaymentDto;
import com.example.springsocial.model.dto.UserPrincipal;
import com.example.springsocial.model.dto.request.NewOrderDto;
import com.example.springsocial.model.enumerated.OrderState;
import com.example.springsocial.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class OrderControllerImpl implements OrderController {

	private final OrderService orderService;

	@Override
	public OrderDto findOrderById(UUID id) {
		return orderService.findOrderById(id);
	}

	@Override
	public List<OrderDto> findAllOrdersForAdmin(String search) {
		return orderService.findAllOrdersForAdmin(search);
	}

	@Override
	public List<OrderDto> findAllOrders(String search) {
		return orderService.findAllOrders(search);
	}

	@Override
	public OrderDto createNewOrder(NewOrderDto orderDto) {
		return orderService.createNewOrder(orderDto);
	}

	@Override
	public OrderDto editOrder(NewOrderDto orderDto, UUID id) {
		return orderService.editOrder(orderDto, id);
	}

	@Override
	public void deleteOrder(UUID id) {
		orderService.deleteOrder(id);
	}

	@Override
	public void changeOrderStatus(UUID order, OrderState state) {
		orderService.changeOrderStatus(order, state);
	}

	@Override
	public List<PaymentDto> findAllPaymentByOrderId(UUID orderId) {
		return orderService.findAllPaymentByOrderId(orderId);
	}


}
