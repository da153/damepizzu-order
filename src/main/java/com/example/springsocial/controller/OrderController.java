package com.example.springsocial.controller;

import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PaymentDto;
import com.example.springsocial.model.dto.Role;
import com.example.springsocial.model.dto.UserPrincipal;
import com.example.springsocial.model.dto.request.NewOrderDto;
import com.example.springsocial.model.dto.request.NewPizzaDto;
import com.example.springsocial.model.enumerated.OrderState;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Tag(name = "Orders")
@RequestMapping("/api/v1/order")
//@SecurityRequirement(name = "bearer")
public interface OrderController {

	@GetMapping("/{id}")
	OrderDto findOrderById(@PathVariable UUID id);

	@GetMapping("/admin/all")
	List<OrderDto> findAllOrdersForAdmin(@RequestParam(value = "search", required = false) String search);

	@GetMapping
	List<OrderDto> findAllOrders(@RequestParam(value = "search", required = false) String search);

	@PostMapping
	OrderDto createNewOrder(@Valid @RequestBody NewOrderDto orderDto);

	@PutMapping("/{id}")
	OrderDto editOrder(@Valid @RequestBody NewOrderDto orderDto, @PathVariable UUID id);

	@DeleteMapping("/{id}")
	void deleteOrder(@PathVariable UUID id);

	@PostMapping("/{id}/changeStatus")
	void changeOrderStatus(@PathVariable("id") UUID order, @RequestParam("state") OrderState state);

	@GetMapping("/{id}/payments")
	List<PaymentDto> findAllPaymentByOrderId(@PathVariable("id")UUID orderId);

}
