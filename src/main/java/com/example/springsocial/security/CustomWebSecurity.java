package com.example.springsocial.security;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.Arrays;

@Slf4j
@AllArgsConstructor
@Configuration
public class CustomWebSecurity extends WebSecurityConfigurerAdapter {

	private final Environment environment;

	private static final String[] AUTH_WHITELIST = {
		// -- swagger ui
		"/swagger-resources", "/swagger-resources/**", "/swagger-ui", "/swagger-ui/**", "/signup", "/webjars/**", "/v3/api-docs/**", "/v3/api-docs.yaml",
		// other public endpoints of your API may be appended to this array
		"/api/public/**", "/api/form/**", "/f/**", "/favicon.ico", "/**/*.png", "/**/*.gif", "/**/*.svg", "/**/*.jpg", "/**/*.html", "/**/*.css", "/**/*.js"};


	@Override
	public void configure(WebSecurity web) {
		web.ignoring()
			.antMatchers(AUTH_WHITELIST)
			.antMatchers(HttpMethod.OPTIONS, "/**");
	}

	@Bean
	public JwtTokenFilter jwtAuthorizationFilter() throws Exception {
		return new JwtTokenFilter(authenticationManager());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf()
			.disable()
			.cors()
			.disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)     //no cookies
			.and()
			.authorizeRequests()
			.antMatchers("/api/v1/order/**/changeStatus").permitAll()
			.anyRequest().authenticated()
			.and()
			.addFilter(jwtAuthorizationFilter());
	}
}

