package com.example.springsocial.service.impl;

import com.example.springsocial.mapper.OrderMapper;
import com.example.springsocial.model.Order;
import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PaymentDto;
import com.example.springsocial.model.dto.request.NewOrderDto;
import com.example.springsocial.model.enumerated.OrderState;
import com.example.springsocial.repository.OrderRepository;
import com.example.springsocial.service.AuthorizationService;
import com.example.springsocial.service.OrderService;
import com.example.springsocial.service.payment.OrderPaymentService;
import com.example.springsocial.validator.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;


@Service
@Slf4j
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

	private final OrderRepository orderRepository;

	private final OrderMapper orderMapper;

	private final Validator<Order> validator;

	private final AuthorizationService authorizationService;

	private final OrderPaymentService orderPaymentService;

	@Override
	public List<OrderDto> findAllOrdersForAdmin(String search) {
		List<Order> all = orderRepository.findAllForAdmin();
		return orderMapper.entitiesToDtos(all);
	}

	@Override
	public OrderDto findOrderById(UUID id) {
		Order order = orderRepository.getNotNull(id);
		return orderMapper.entityToDto(order);
	}

	@Override
	public List<OrderDto> findAllOrders(String search) {
		List<Order> orders = orderRepository.getOrdersByCustomerId(principal.getAccount()
			.getId());
		return orderMapper.entitiesToDtos(orders);
	}

	@Override
	public OrderDto createNewOrder(NewOrderDto orderDto) {
		Order order = orderMapper.createNewOrder(orderDto);
		validator.validateBeforeCreation(order);
		order = orderRepository.save(order);
		return orderMapper.entityToDto(order);
	}

	@Override
	public OrderDto editOrder(NewOrderDto orderDto, UUID id) {
		Order toEdit = orderRepository.getNotNull(id);
		Order editBy = orderMapper.createNewOrder(orderDto);
		orderMapper.edit(toEdit, editBy);
		validator.validateBeforeEditing(toEdit);
		Order edited = orderRepository.save(toEdit);
		return orderMapper.entityToDto(edited);
	}

	@Override
	public void deleteOrder(UUID id) {
		orderRepository.deleteById(id);
	}

	@Override
	public void changeOrderStatus(UUID id, OrderState state) {
		authorizationService.sendPushNotification(principal.getToken(),
			"🍕 Objednávka",
			notificationMessage(state)
		);

		Order orderDbs = orderRepository.getById(id);
		orderDbs.setState(state);
		orderRepository.save(orderDbs);
	}

	private String notificationMessage(OrderState state) {
		switch (state) {
			case DONE:
				return "Objednávka je hotova!";
			case PAID:
				return "Objednávka byla zaplacena.";
			case COOKING:
				return "Objednávka se právě peče.";
			case DELIVERY:
				return "Objenávka byla předána kurýrovi.";
			case CREATED:
				return "Objednávka byla vytvořena.";
			case CANCELED:
				return "Objednávka byla zrušena.";
			default:
				return "";
		}
	}

	@Override
	public List<PaymentDto> findAllPaymentByOrderId(UUID orderId) {
		return orderPaymentService.findAllPaymentByOrderId(orderId);
	}

}
