package com.example.springsocial.service;

import com.example.springsocial.model.dto.FileDto;
import com.example.springsocial.model.dto.PageDto;
import com.example.springsocial.model.enumerated.GeneralSortType;
import com.example.springsocial.util.Tuple;
import io.minio.ObjectStat;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface FileService {

	FileDto getFile(UUID id);

	FileDto uploadFile(MultipartFile file);

	void deleteFile(UUID id);

	PageDto<FileDto> getAllFiles(Integer pageNumber, Integer pageSize, String searchQuery, GeneralSortType sortType);

	Tuple<byte[], ObjectStat> serveFile(UUID id);

}
