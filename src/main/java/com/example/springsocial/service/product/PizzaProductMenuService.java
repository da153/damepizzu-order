package com.example.springsocial.service.product;

import com.example.springsocial.model.dto.productMenuApi.ProductMenuPizzaDto;

import java.util.List;
import java.util.UUID;

public interface PizzaProductMenuService {

	ProductMenuPizzaDto isPizzaExists(UUID pizzaId);

	void arePizzasExists(List<UUID> pizzaIds);

	List<ProductMenuPizzaDto> findPizzasByListOfIds(List<UUID> ids);
}
