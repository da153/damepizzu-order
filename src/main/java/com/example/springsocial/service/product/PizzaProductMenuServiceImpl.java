package com.example.springsocial.service.product;

import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.dto.productMenuApi.ProductMenuPizzaDto;
import com.example.springsocial.service.api.product.PizzaProductMenuApiClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class PizzaProductMenuServiceImpl implements PizzaProductMenuService {

	private final PizzaProductMenuApiClient apiClient;

	@Override
	public ProductMenuPizzaDto isPizzaExists(UUID pizzaId) {
		Call<ProductMenuPizzaDto> pizzaCall = apiClient.findPizzaById(pizzaId);
		try {
			Response<ProductMenuPizzaDto> response = pizzaCall.execute();
			return response.body();
		} catch (Exception ex) {
			throw new BadRequestException("assa");
		}
	}

	@Override
	public void arePizzasExists(List<UUID> pizzaIds) {

	}

	@Override
	public List<ProductMenuPizzaDto> findPizzasByListOfIds(List<UUID> ids) {
		List<String> stringIds = ids.stream()
			.map(UUID::toString)
			.collect(Collectors.toList());
		Call<List<ProductMenuPizzaDto>> call = apiClient.findPizzasByIds(stringIds.toArray(new String[0]));
		Response<List<ProductMenuPizzaDto>> response = null;
		try {
			response = call.execute();
			System.out.println("");
		} catch (IOException e) {
			throw new BadRequestException("Order cannot be created1");
		}
		if (!response.isSuccessful()) {
			throw new BadRequestException("Pizza ids are not valid, or item does not exists");
		}
		return response.body();
	}

}
