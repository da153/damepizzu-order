package com.example.springsocial.service;

import com.example.springsocial.config.auth.TokenAuthorizationClient;
import com.example.springsocial.exception.UserPropagableException;
import com.example.springsocial.model.dto.AccountDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

@Service
@AllArgsConstructor
public class AuthorizationService {

	private final TokenAuthorizationClient tokenAuthorizationClient;

	public AccountDto getAccountFromToken(String token) {
		Call<AccountDto> call = tokenAuthorizationClient.getMyAccountFromToken(token);
		try {
			Response<AccountDto> response = call.execute();
			if (response.isSuccessful()) {
				return response.body();
			}
			throw new UserPropagableException("Cannot get user by this token!");
		} catch (Exception e) {
			throw new UserPropagableException("Cannot get user by this token!");
		}
	}

	public void sendPushNotification(String token, String title, String message) {
		Call<Void> call = tokenAuthorizationClient.sendNotification(token, title, message);
		try {
			call.execute();
		} catch (Exception e) {
			throw new UserPropagableException("Cannot send push notification");
		}
	}

}
