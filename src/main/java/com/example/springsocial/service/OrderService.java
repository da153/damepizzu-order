package com.example.springsocial.service;

import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PaymentDto;
import com.example.springsocial.model.dto.UserPrincipal;
import com.example.springsocial.model.dto.request.NewOrderDto;
import com.example.springsocial.model.enumerated.OrderState;
import com.example.springsocial.model.enumerated.Role;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.UUID;

public interface OrderService {

	UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext()
		.getAuthentication()
		.getPrincipal();
	
	@Secured(Role.MANAGER_STRING)
	public List<OrderDto> findAllOrdersForAdmin(String search);

	@Secured({ Role.MANAGER_STRING, Role.CASHIER_STRING })
	OrderDto findOrderById(UUID id);

	@Secured(Role.CASHIER_STRING)
	List<OrderDto> findAllOrders(String search);

	OrderDto createNewOrder(NewOrderDto orderDto);

	@Secured(Role.MANAGER_STRING)
	OrderDto editOrder(NewOrderDto orderDto, UUID id);

	@Secured(Role.MANAGER_STRING)
	void deleteOrder(UUID id);


	void changeOrderStatus(UUID id, OrderState state);

	@Secured(Role.MANAGER_STRING)
	List<PaymentDto> findAllPaymentByOrderId(UUID orderId);

}
