package com.example.springsocial.service.api.product;

import com.example.springsocial.model.dto.productMenuApi.ProductMenuPizzaDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;
import java.util.UUID;

public interface PizzaProductMenuApiClient {

	@GET("users/{user}/repos")
	Call<List<Object>> listRepos(@Path("user") String user);

	@GET("/api/v1/pizza/{id}")
	Call<ProductMenuPizzaDto> findPizzaById(@Path("id") UUID user);

	@GET("/api/v1/pizza/all")
	Call<List<ProductMenuPizzaDto>> findPizzasByIds(@Query("pizzaId") String[] pizzaIds);
}
