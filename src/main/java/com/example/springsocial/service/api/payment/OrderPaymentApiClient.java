package com.example.springsocial.service.api.payment;

import com.example.springsocial.model.dto.PaymentDto;
import com.example.springsocial.model.dto.productMenuApi.ProductMenuPizzaDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;
import java.util.UUID;

public interface OrderPaymentApiClient {

	@Headers("Accept: application/json")
	@GET("/api/v1/payment/findAllPaymentByOrderId/{id}")
	Call<List<PaymentDto>> findAllPaymentByOrderId(@Path("id") UUID order);

}
