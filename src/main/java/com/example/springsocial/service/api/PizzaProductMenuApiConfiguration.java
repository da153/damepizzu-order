package com.example.springsocial.service.api;

import com.example.springsocial.config.MicroserviceApiConfiguration;
import com.example.springsocial.service.api.product.PizzaProductMenuApiClient;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
@AllArgsConstructor
public class PizzaProductMenuApiConfiguration {

	private final MicroserviceApiConfiguration apiConfiguration;

	@Bean
	public PizzaProductMenuApiClient createProductMenuApiClient(){
		Retrofit retrofit = new Retrofit.Builder()
			.baseUrl(apiConfiguration.getMenu())
			.addConverterFactory(GsonConverterFactory.create())
			.build();
		return retrofit.create(PizzaProductMenuApiClient.class);
	}


}
