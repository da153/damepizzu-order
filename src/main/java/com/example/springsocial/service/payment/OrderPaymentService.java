package com.example.springsocial.service.payment;

import com.example.springsocial.model.dto.PaymentDto;
import com.example.springsocial.model.dto.productMenuApi.ProductMenuPizzaDto;

import java.util.List;
import java.util.UUID;

public interface OrderPaymentService {

	List<PaymentDto> findAllPaymentByOrderId(UUID orderId);

}
