package com.example.springsocial.service.payment;

import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.dto.PaymentDto;
import com.example.springsocial.model.dto.productMenuApi.ProductMenuPizzaDto;
import com.example.springsocial.service.api.payment.OrderPaymentApiClient;
import com.example.springsocial.service.api.product.PizzaProductMenuApiClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class OrderPaymentServiceImpl implements OrderPaymentService {

	private final OrderPaymentApiClient apiClient;


	@Override
	public List<PaymentDto> findAllPaymentByOrderId(UUID orderId) {
		Call<List<PaymentDto>> call = apiClient.findAllPaymentByOrderId(orderId);
		Response<List<PaymentDto>> response = null;
		try {
			response = call.execute();
		} catch (IOException e) {
			throw new BadRequestException("Order cannot be found");
		}
		if (!response.isSuccessful()) {
			throw new BadRequestException("Order cannot be fetched");
		}
		return response.body();
	}
}
