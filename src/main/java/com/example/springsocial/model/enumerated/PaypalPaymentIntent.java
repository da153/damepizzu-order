package com.example.springsocial.model.enumerated;

public enum PaypalPaymentIntent {

	sale, authorize, order

}
