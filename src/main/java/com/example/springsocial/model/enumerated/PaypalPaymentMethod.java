package com.example.springsocial.model.enumerated;


public enum PaypalPaymentMethod {

	credit_card, paypal

}
