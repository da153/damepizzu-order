package com.example.springsocial.model.enumerated;

public enum FileType {
	IMAGE, FILE
}
