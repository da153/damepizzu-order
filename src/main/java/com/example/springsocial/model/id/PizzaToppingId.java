package com.example.springsocial.model.id;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class PizzaToppingId implements Serializable {

	@Column(name = "pizza_id")
	UUID pizzaId;

	@Column(name = "topping_id")
	UUID toppingId;
}
