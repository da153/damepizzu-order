package com.example.springsocial.model.dto;

import com.example.springsocial.model.enumerated.PaymentState;
import com.example.springsocial.model.enumerated.PaypalPaymentIntent;
import com.example.springsocial.model.enumerated.PaypalPaymentMethod;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDto {

	private Integer id;

	private String externalId;

	private String description;

	private String currency;

	private double price;

	private String failureReason;

	private LocalDateTime createdAt;

	private LocalDateTime paidAt;

	@Enumerated(value = EnumType.STRING)
	private PaypalPaymentMethod method;

	@Enumerated(value = EnumType.STRING)
	private PaypalPaymentIntent intent;

	@Enumerated(value = EnumType.STRING)
	private PaymentState state;

	private UUID orderId;

}
