package com.example.springsocial.model.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class UserPrincipal implements UserDetails {
	private AccountDto account;
	private String token;
	public UserPrincipal() {
	}
	public UserPrincipal(AccountDto user, String token) {
		super();
		account = user;
		this.token = token;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return List.of(new SimpleGrantedAuthority(account.getRole().getRoleString()));
	}

	@Override
	public String getPassword() {
		return "";
	}
	@Override
	public String getUsername() {
		return account.getEmail();
	}
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return true;
	}

	public AccountDto getAccount() {
		return account;
	}

	public String getToken(){ return token;}



}
