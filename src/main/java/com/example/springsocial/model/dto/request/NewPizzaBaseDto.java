package com.example.springsocial.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewPizzaBaseDto {

	private String name;

	private String description;

	private Double extraPrice;
}
