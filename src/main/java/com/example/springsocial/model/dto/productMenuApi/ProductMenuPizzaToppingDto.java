package com.example.springsocial.model.dto.productMenuApi;

import com.example.springsocial.model.dto.FileDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductMenuPizzaToppingDto {
	private UUID id;

	private String name;

	private String description;

	private Double price;

	private FileDto image;
}
