package com.example.springsocial.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewOrderDto {

	private OrderCustomerDto customer;
	private List<OrderItemDto> items;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class OrderItemDto {
		private UUID pizzaId;
		private Integer amount;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class OrderCustomerDto {
        private int id;
		private String fullname;
		private String address;
		private String zip;
		private String city;
		private String email;
		private String phone;
	}

}

