package com.example.springsocial.model.dto;

import com.example.springsocial.model.Order;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDto {

	private UUID id;

	private String name;

	private String description;

	private Double price;

	private Integer amount;

	private UUID pizzaId;

	@JsonIgnore
	private Order order;
}
