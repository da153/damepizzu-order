package com.example.springsocial.model.dto.productMenuApi;


import com.example.springsocial.model.dto.FileDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductMenuPizzaDto {

	private UUID id;

	private String name;

	private String description;

	private Double price;

	private List<PizzaMenuPizzaBaseDto> toppings;

	private PizzaMenuPizzaBaseDto pizzaBase;

	private FileDto image;
}
