package com.example.springsocial.model.converter;

import com.example.springsocial.model.enumerated.FileType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class FileTypeConverter implements AttributeConverter<FileType, String> {

	@Override
	public String convertToDatabaseColumn(FileType attribute) {
		return attribute.toString()
			.toLowerCase();
	}

	@Override
	public FileType convertToEntityAttribute(String dbData) {
		return FileType.valueOf(dbData.toUpperCase());
	}

}
