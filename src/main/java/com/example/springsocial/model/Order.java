package com.example.springsocial.model;

import com.example.springsocial.model.enumerated.OrderState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
		@Id
		@GeneratedValue(generator = "UUID")
		@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
		@Column(name = "id", updatable = false, nullable = false)
		private UUID id;

		private Double totalPrice;

		@Embedded
		private OrderCustomer customer;

		@Enumerated(EnumType.STRING)
		private OrderState state;

		private LocalDateTime createdAt;

		@OneToMany(fetch = FetchType.LAZY, mappedBy = "order",  cascade = CascadeType.PERSIST)
		private List<OrderItem> orderItems;

		public void prepareSave() {
			orderItems = orderItems.stream()
				.peek(o -> o.setOrder(this))
				.collect(Collectors.toList());
		}

}
