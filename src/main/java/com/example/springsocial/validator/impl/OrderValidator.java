package com.example.springsocial.validator.impl;

import com.example.springsocial.model.Order;
import com.example.springsocial.validator.Validator;
import org.springframework.stereotype.Component;

@Component
public class OrderValidator implements Validator<Order> {

	@Override
	public void validateBeforeCreation(Order object) {

	}

	@Override
	public void validateBeforeEditing(Order object) {

	}

}
