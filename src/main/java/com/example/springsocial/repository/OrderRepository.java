package com.example.springsocial.repository;

import com.example.springsocial.model.Order;

import java.util.List;
import java.util.UUID;

public interface OrderRepository extends BaseRepository<Order, UUID> {

	List<Order> getOrdersByCustomerId(int id);

	List<Order> findAllForAdmin();
}
