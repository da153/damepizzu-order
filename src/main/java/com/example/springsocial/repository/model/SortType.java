package com.example.springsocial.repository.model;

import org.springframework.data.domain.Sort;

import java.util.List;

public interface SortType {

	List<Sort.Order> getSortOrders();

	Sort.Direction getDirection();

}
