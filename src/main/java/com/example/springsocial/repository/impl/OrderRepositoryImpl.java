package com.example.springsocial.repository.impl;

import com.example.springsocial.model.Order;
import com.example.springsocial.repository.OrderRepository;
import com.example.springsocial.repository.engine.OrderRepositoryEngine;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@AllArgsConstructor
public class OrderRepositoryImpl extends AbstractRepository<Order, UUID> implements OrderRepository {

	private final OrderRepositoryEngine engine;

	@Override
	protected CrudRepository<Order, UUID> getRepositoryEngine() {
		return engine;
	}


	@Override
	public List<Order> getOrdersByCustomerId(int id) {
		return engine.findAllByCustomerId(id);
	}

	@Override
	public List<Order> findAllForAdmin() {
		return engine.findAllOrderByNewest();
	}

}
