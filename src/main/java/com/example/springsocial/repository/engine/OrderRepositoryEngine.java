package com.example.springsocial.repository.engine;

import com.example.springsocial.model.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface OrderRepositoryEngine extends CrudRepository<Order, UUID> {

	@Query("SELECT o FROM Order o where o.customer.customerId = ?1")
	List<Order> findAllByCustomerId(int id);


	@Query("SELECT o FROM Order o ORDER BY o.createdAt DESC")
	List<Order> findAllOrderByNewest();

}
