package com.example.springsocial.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@NoArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "app.microservice")
public class MicroserviceApiConfiguration {

	private String menu;
	private String auth;
	private String payment;
}
