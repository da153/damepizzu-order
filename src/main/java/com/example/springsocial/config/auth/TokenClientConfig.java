package com.example.springsocial.config.auth;

import com.example.springsocial.config.MicroserviceApiConfiguration;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
@AllArgsConstructor
public class TokenClientConfig {

	private final MicroserviceApiConfiguration apiConfiguration;

	@Bean
	public TokenAuthorizationClient createAuthorizationClient() {
		Retrofit retrofit = new Retrofit.Builder().baseUrl(apiConfiguration.getAuth())
			.addConverterFactory(GsonConverterFactory.create())
			.build();
		return retrofit.create(TokenAuthorizationClient.class);
	}

}
