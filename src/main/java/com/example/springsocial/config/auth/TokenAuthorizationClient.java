package com.example.springsocial.config.auth;

import com.example.springsocial.model.dto.AccountDto;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface TokenAuthorizationClient {

	@GET("/api/account/me")
	Call<AccountDto> getMyAccountFromToken(@Header("Authorization") String token);

	@FormUrlEncoded
	@POST("/api/account/push-notification")
	Call<Void> sendNotification(@Header("Authorization") String token,
	                                @Field("title") String title,
	                                @Field("message") String message);

}
