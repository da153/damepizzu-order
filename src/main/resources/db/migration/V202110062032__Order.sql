CREATE SCHEMA IF NOT EXISTS "orders";

CREATE TABLE "orders"."order"
(
    "id"    uuid              NOT NULL,
    "total_price"  numeric NOT NULL default 0,
    "customer_fullname" character  varying NOT NULL,
    "customer_zip" character  varying NOT NULL,
    "customer_city" character  varying NOT NULL,
    "customer_address" character  varying NOT NULL,
    "customer_email" character  varying NOT NULL,
    "customer_phone" character  varying NOT NULL,
    "state"  character varying NOT NULL,
    "created_at" timestamp NOT NULL DEFAULT now(),
    CONSTRAINT "order_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE TABLE "orders"."order_item"
(
    "id"    uuid              NOT NULL,
    "name"  character  varying NOT NULL,
    "description" character  varying NOT NULL,
    "price" numeric NOT NULL DEFAULT 0,
    "amount" integer NOT NULL DEFAULT 1,
    "pizza_id" uuid NOT NULL,
    "order_id" uuid NOT NULL,
    CONSTRAINT "order_item_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

ALTER TABLE "orders"."order_item"
    ADD FOREIGN KEY ("order_id") REFERENCES "orders"."order" ("id") ON DELETE CASCADE;

