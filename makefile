# Include environment variables if possible
-include .env
#-include .env.production
TIME := $(shell date +"%Y%m%d%H%M%S")

migrate:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations -outOfOrder=true migrate

migrate-down:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations undo

migrate-drop:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations clean

migrate-status:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations info

migrate-reset: migrate-drop migrate

migrate-create:
	touch src/main/resources/db/migration/V${TIME}__RENAME.sql

